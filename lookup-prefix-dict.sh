INPUT_SYMBOLS_S="isyms3.txt"
OUTPUT_SYMBOLS_S="osyms3.txt"

WORD_TEXT_FST="word_text3.fst"
WORD_BINARY_FST="word_binary3.fst"

WORDS=$2
LETTERS_PHONE_MAPPING_LEX_WITH_PARTIAL=$1

COMPOSED_PREFIX="out3.fst"
COMPOSED_PREFIX_DET_RE_SORT="out3_det_rm_sort.fst"
COMPOSED_PREFIX_SORT="out3_sort.fst"
prev_state=0
next_state=1

LEN_WORD=$(( ${#WORDS} - 1))


echo '' > $WORD_TEXT_FST
for i in `seq 0 $LEN_WORD`;
do
    echo $prev_state $next_state  "<epsilon>" ${WORDS:i:1} >> $WORD_TEXT_FST
    prev_state=$next_state
    next_state=$(( $next_state+1 ))
done
echo $prev_state >> $WORD_TEXT_FST

fstcompile --isymbols=$OUTPUT_SYMBOLS_S --osymbols=$OUTPUT_SYMBOLS_S --keep_isymbols --keep_osymbols $WORD_TEXT_FST $WORD_BINARY_FST 2>/dev/null

if [ $? -eq 0 ]
then 
    fstcompose $WORD_BINARY_FST $LETTERS_PHONE_MAPPING_LEX_WITH_PARTIAL $COMPOSED_PREFIX 2>/dev/null
    # fsttopsort $COMPOSED_PREFIX $COMPOSED_PREFIX_SORT
    # fstrmepsilon $COMPOSED_PREFIX | fstdeterminize - | fstrmepsilon - |fsttopsort - $COMPOSED_PREFIX_DET_RE_SORT

    command_op=$(fstprint --isymbols=$OUTPUT_SYMBOLS_S --osymbols=$OUTPUT_SYMBOLS_L $COMPOSED_PREFIX)
    # echo $command_op
    python3 lookup-prefix-dict.py $command_op ${#WORDS}
else
    echo "<OOV>"
fi



