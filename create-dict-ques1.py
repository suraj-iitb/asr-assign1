import sys
import re

I_F_STATE = str(0)
state = 1

input_label = 1
output_label = 1

input = ''
output = ''
trans = ''

phones_list = []

eps = "<epsilon>"
eps_label = eps + " 0\n"

with open(sys.argv[1]) as f1:
    with open(sys.argv[2], 'w') as f2:
        with open(sys.argv[3], 'w') as f3:
            with open(sys.argv[4], 'w') as f4:
                pass
                
with open(sys.argv[1]) as f1:
    with open(sys.argv[2], 'a') as f2:
        with open(sys.argv[3], 'a') as f3:
            with open(sys.argv[4], 'a') as f4:

                # write eps label to inp and output symbol table file
                f3.write(eps_label)
                f4.write(eps_label)

                for line in f1:
                    # finding a word and its phone             
                    line = re.split(r'\s', line.strip())
                    word = line[0]
                    phones = line[1:]

                    # test 
                    # f = open('temp.txt', 'a')
                    # f.write(str(line)+'\n')
                    

                    # to handle the case when phone is of length 1
                    if len(phones) == 1:
                        phone = phones[0]

                        trans = I_F_STATE + " " + I_F_STATE + " " + word + " " + phone + "\n"

                        input = word + ' ' + str(input_label) + '\n'
                        input_label += 1

                        phones_absent = False
                        try:
                            phones_list.index(phone)
                        except ValueError:
                            phones_absent = True

                        if phones_absent :
                            phones_list.append(phone)

                            output = phone + ' ' + str(output_label) + '\n'
                            output_label += 1
                            f4.write(output)

                        f2.write(trans)
                        f3.write(input)
                        
                        
                    # when a word's phone length is greater than 1 
                    else:
                        for phone_idx, phone in enumerate(phones):
                            phones_absent = False
                            try:
                                phones_list.index(phone)
                            except ValueError:
                                phones_absent = True

                            # to handle 1st phone in phones list
                            if phone_idx == 0:
                                trans = I_F_STATE + " " + str(state) + " " + word + " " + phone + "\n"
                                state += 1

                                input = word + ' ' + str(input_label) + '\n'
                                input_label += 1

                                if phones_absent :
                                    phones_list.append(phone)

                                    output = phone + ' ' + str(output_label) + '\n'
                                    output_label += 1
                                    f4.write(output)
                                        
                                f2.write(trans)
                                f3.write(input)
                                
                                
                            else:
                                # to handle last phone
                                if phone_idx == len(phones)-1:
                                    trans = str(state-1) + " " + I_F_STATE + " " + eps + " " + phone + "\n"
                                else:
                                    trans = str(state-1) + " " + str(state) + " " + eps + " " + phone + "\n"
                                    state += 1
                                
                                if phones_absent :
                                    phones_list.append(phone)

                                    output = phone + ' ' + str(output_label) + '\n'
                                    output_label += 1
                                    f4.write(output)
                                
                                f2.write(trans)
                                

                    # whenever a word and its phones is processed write a newline for readability in files      
                    # f2.write("\n")
                    # f3.write("\n")
                    # f4.write("\n")

                # write final state and its weight to text.fst
                f2.write(I_F_STATE)


