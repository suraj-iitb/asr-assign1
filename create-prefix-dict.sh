INPUT_SYMBOLS_S="isyms3.txt"
OUTPUT_SYMBOLS_S="osyms3.txt"

INPUT_SYMBOLS_L="isyms1.txt"
OUTPUT_SYMBOLS_L="osyms1.txt"

TEXT_FST_S="text3.fst"
BINARY_FST_S="S3.fst"
BINARY_FST_S_INV="S_inv3.fst"

BINARY_FST_Q=$3 #QPrefix.fst
BINARY_FST_L=$2


WORD_PHONE_MAPPING_LEX=$1

python3 create-dict-ques3.py $WORD_PHONE_MAPPING_LEX $TEXT_FST_S $INPUT_SYMBOLS_S $OUTPUT_SYMBOLS_S
fstcompile --isymbols=$INPUT_SYMBOLS_S --osymbols=$OUTPUT_SYMBOLS_S --keep_isymbols --keep_osymbols $TEXT_FST_S $BINARY_FST_S  2>/dev/null
fstinvert $BINARY_FST_S $BINARY_FST_S_INV 2>/dev/null
fstcompose $BINARY_FST_S_INV $BINARY_FST_L "temp1.fst" 2>/dev/null

fstpush --push_labels "temp1.fst" "temp2.fst"
fstrmepsilon "temp2.fst" $BINARY_FST_Q
