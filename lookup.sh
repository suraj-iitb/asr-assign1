INPUT_SYMBOLS="isyms1.txt"
OUTPUT_SYMBOLS="osyms1.txt"

WORD=$2
LEXICON=$1
WORD_PHONE_MAPPING_LEX=$LEXICON

WORD_TEXT_FST="word_text1.fst"
WORD_BINARY_FST="word_binary1.fst"

COMPOSED_FST="out1.fst"

echo 0 1 $WORD $WORD > $WORD_TEXT_FST
echo 1 >> $WORD_TEXT_FST

fstcompile --isymbols=$INPUT_SYMBOLS --osymbols=$INPUT_SYMBOLS --keep_isymbols --keep_osymbols $WORD_TEXT_FST $WORD_BINARY_FST  2>/dev/null

if [ $? -eq 0 ]
then 
    fstcompose $WORD_BINARY_FST $WORD_PHONE_MAPPING_LEX $COMPOSED_FST
    command_op=$(fstprint --isymbols=$INPUT_SYMBOLS --osymbols=$OUTPUT_SYMBOLS $COMPOSED_FST)
    python3 lookup.py $command_op
else
    echo "<OOV>"
fi
