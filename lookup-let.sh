INPUT_SYMBOLS_S="isyms2.txt"
OUTPUT_SYMBOLS_S="osyms2.txt"

INPUT_SYMBOLS_L="isyms1.txt"
OUTPUT_SYMBOLS_L="osyms1.txt"

LETTERS=$2
LETTERS_PHONE_MAPPING_LEX=$1

WORD_TEXT_FST="word_text2.fst"
WORD_BINARY_FST="word_binary2.fst"

COMPOSED_FST="out2.fst"
COMPOSED_FST_WITHOUT_E="out2_rme.fst"

prev_state=0
next_state=1

echo '' > $WORD_TEXT_FST
for letter in $LETTERS;
do
    echo $prev_state $next_state  "<epsilon>" $letter >> $WORD_TEXT_FST
    prev_state=$next_state
    next_state=$(( $next_state+1 ))
done
echo $prev_state >> $WORD_TEXT_FST



fstcompile --isymbols=$OUTPUT_SYMBOLS_S --osymbols=$OUTPUT_SYMBOLS_S --keep_isymbols --keep_osymbols $WORD_TEXT_FST $WORD_BINARY_FST  2>/dev/null

if [ $? -eq 0 ]
then 
    fstcompose $WORD_BINARY_FST $LETTERS_PHONE_MAPPING_LEX $COMPOSED_FST
    fstrmepsilon $COMPOSED_FST $COMPOSED_FST_WITHOUT_E
    command_op=$(fstprint --isymbols=$OUTPUT_SYMBOLS_S --osymbols=$OUTPUT_SYMBOLS_L $COMPOSED_FST_WITHOUT_E)
    python3 lookup.py $command_op
else
    echo "<OOV>"
fi


