INPUT_SYMBOLS="isyms1.txt"
OUTPUT_SYMBOLS="osyms1.txt"

TEXT_FST="text1.fst"
BINARY_FST=$2

WORD_PHONE_MAPPING_LEX=$1

python3 create-dict-ques1.py $WORD_PHONE_MAPPING_LEX $TEXT_FST $INPUT_SYMBOLS $OUTPUT_SYMBOLS
fstcompile --isymbols=$INPUT_SYMBOLS --osymbols=$OUTPUT_SYMBOLS --keep_isymbols --keep_osymbols $TEXT_FST $BINARY_FST  2>/dev/null